<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdpenimbanganBalitaToBalita extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balita', function (Blueprint $table) {
            //
        $table->unsignedBigInteger('idpenimbangan_balita');
        $table->foreign('idpenimbangan_balita')->references('id')->on('balita');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balita', function (Blueprint $table) {
            //
        $table->dropForeign(['idpenimbangan_balita']);
        $table->dropColumn(['idpenimbangan_balita']);
        });
    }
}
