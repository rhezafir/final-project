<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdibuhamilToTambahDarah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tambah_darah', function (Blueprint $table) {
            //
        $table->unsignedBigInteger('idibuhamil');
        $table->foreign('idibuhamil')->references('id')->on('tambah_darah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tambah_darah', function (Blueprint $table) {
            //
        $table->dropForeign(['idibuhamil']);
        $table->dropColumn(['idibuhamil']);
        });
    }
}
