<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdpenimbanganBalitaToTambahDarah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tambah_darah', function (Blueprint $table) {
            //
        $table->unsignedBigInteger('idpenimbangan_balita');
        $table->foreign('idpenimbangan_balita')->references('id')->on('tambah_darah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tambah_darah', function (Blueprint $table) {
            //
        $table->dropForeign(['idpenimbangan_balita']);
        $table->dropColumn(['idpenimbangan_balita']);
        });
    }
}
