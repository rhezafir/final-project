<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIbuhamilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ibuhamil', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_ibu',150);
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->string('nama_suami',150);
            $table->string('alamat',255);
            $table->string('tanggal_daftar');
            $table->string('umur_kehamilan');
            $table->string('berat_badan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ibuhamil');
    }
}
