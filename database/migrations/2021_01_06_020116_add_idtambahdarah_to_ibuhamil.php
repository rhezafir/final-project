<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdtambahdarahToIbuhamil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ibuhamil', function (Blueprint $table) {
            //
        $table->unsignedBigInteger('idtambahdarah');
        $table->foreign('idtambahdarah')->references('id')->on('ibuhamil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ibuhamil', function (Blueprint $table) {
            //
        $table->dropForeign(['idtambahdarah']);
        $table->dropColumn(['idtambahdarah']);
        });
    }
}
