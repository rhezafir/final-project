@extends ('crud.layoutmaster')

@section ('title')
  Tabel Balita
@endsection

@section ('content')
@if (session('success'))
	<div class="alert alert-success">
		{{ session ('success')}}
	</div>
@endif
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">#</th>
      <th>Nama Anak</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
      <th>Jenis Kelamin</th>
      <th>Nama Ayah</th>
      <th>Nama Ibu</th>
      <th>Alamat</th>
      <th>Umur</th>
      <th style="width: 40px">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($posts as $key => $balita)
    <tr>
      <td> {{ $key + 1}} </td>
      <td> {{ $balita -> nama_anak }} </td>
      <td> {{ $balita -> tempat_lahir }} </td>
      <td> {{ $balita -> tanggal_lahir }} </td>
      <td> {{ $balita -> jenis_kelamin }} </td>
      <td> {{ $balita -> nama_ayah }} </td>
      <td> {{ $balita -> nama_ibu }} </td>
      <td> {{ $balita -> alamat }} </td>
      <td> {{ $balita -> umur }} </td>
      <td style="display: flex;"> 
        <a href="/balita/{{$balita->id}}" class="btn btn-info btn-sm btn-success"> Detail </a> 
        <a href="/balita/{{$balita->id}}/edit" class="btn btn-info btn-sm btn-success btn-warning ml-2"> Edit </a>
        <form action="/balita/{{$balita->id}}" method="post">
        @csrf
        @method('DELETE')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2"> 
      </td> 

    </tr>
    @endforeach
  

</table>
@endsection




     