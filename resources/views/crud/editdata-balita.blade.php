@extends ('crud.layoutmaster')

@section ('title')
  Balita
@endsection

@section ('content')
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Post Ke - {{$post->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/balita/{{$post->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama_anak">Nama Anak</label>
                    <input type="text" class="form-control" id="nama_anak" name="nama_anak" value="{{old('nama_anak',$post->nama_anak)}}" placeholder="Nama">
                     @error('nama_anak')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="{{old('tempat_lahir',$post->tempat_lahir)}}" placeholder="Tempat Lahir">
                     @error('tempat_lahir')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="{{old('tanggal_lahir',$post->tanggal_lahir)}}" placeholder="Tanggal Lahir">
                     @error('tanggal_lahir')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="jenis_kelamin">Jenis Kelamin</label>
                    <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="{{old('jenis_kelamin',$post->jenis_kelamin)}}" placeholder="Jenis Kelamin">
                     @error('jenis_kelamin')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="nama_ayah">Nama Ayah</label>
                    <input type="text" class="form-control" id="nama_ayah" name="nama_ayah" value="{{old('nama_ayah',$post->nama_ayah)}}" placeholder="Nama Ayah">
                     @error('nama_ayah')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="nama_ayah">Nama Ibu</label>
                    <input type="text" class="form-control" id="nama_ibu" name="nama_ibu" value="{{old('nama_ibu',$post->nama_ibu)}}" placeholder="Nama Ibu">
                     @error('nama_ibu')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" value="{{old('alamat',$post->alamat)}}" placeholder="Alamat">
                     @error('alamat')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" value="{{old('umur',$post->umur)}}" placeholder="Umur">
                     @error('umur')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                   <button type="cancel" class="btn btn-danger btn-primary">Cancel</button>
                </div>
              </form>
            </div>
  </div>

  @endsection