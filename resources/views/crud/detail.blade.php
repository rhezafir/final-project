@extends ('crud.layoutmaster')

@section ('title')
  Detail Data Balita
@endsection

@section ('content')
  <h4> {{ $post -> nama_anak }} </h4>
  <p> {{ $post -> tempat_lahir }} </p>
  <p> {{ $post -> tanggal_lahir }} </p>
  <p> {{ $post -> jenis_kelamin}} </p>
  <p> {{ $post -> nama_ayah }} </p>
  <p> {{ $post -> nama_ibu }} </p>
  <p> {{ $post -> alamat }} </p>
  <p> {{ $post -> umur }} </p>
@endsection