@extends ('layout.master')

@section ('title')
  <h1> Welcome! </h1>
@endsection

 @section ('content')
 <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-20">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"></h5>

                <p class="card-text">
                  <div class="d-flex justify-content-center" >
                    <div id="carouselExampleControls" class="carousel slide mb-3" data-ride="carousel">
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img src="img/bg1.jpg" class="d-block m-auto" width="800px" height="400px" alt="...">
                        </div>
                        <div class="carousel-item ">
                          <img src="img/bg5.jpg" class="d-block m-auto" width="800px" height="400px" alt="...">
                        </div>  
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>

                      <div class="card card-primary card-outline col-ml-5">
                      <div class="card-header">
                        <h5 class="card-title m-0">News</h5>
                      </div>
                      <div class="card-body">
                        <h6 class="card-title"> Apa itu posyandu ?</h6>

                        <p class="card-text">suatu wadah komunikasi alih teknologi dalam pelayanan kesehatan masyarakat dari Keluarga Berencana dari masyarakat, oleh masyarakat dan untuk masyarakat dengan dukungan pelayanan serta pembinaan teknis dari petugas kesehatan dan keluarga. berencana yang mempunyai nilai strategis untuk pengembangan sumber daya manusia sejak dini</p>
                        <a href="https://www.kajianpustaka.com/2013/07/posyandu.html" class="btn btn-primary"> Selengkapnya </a>
                      </div>
                      </div>
                    </div>

                </p>
              </div>
            </div>

          <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>

         <div class="row">
          <div class="col-lg-20">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"></h5>

                <p class="card-text">
                  <div class="d-flex justify-content-center" >
                    
                      <div class="card card-primary card-outline">
                      <div class="card-header">
                        <h5 class="card-title m-0">Kamu Harus Tahu!</h5>
                      </div>
                      <div class="card-body">
                        <h6 class="card-title"> Mengapa posyandu penting bagi balita ? </h6>

                        <p class="card-text">Pakar kesehatan menyebutkan bahwa tujuan utama dari Posyandu adalah agar mensejahterakan masyarakat, khususnya bagi ibu dan anak. Berikut adalah beberapa manfaatnya yaitu :<br>

                        1. Perbaikan gizi <br>

                        Sebagaimana program Kementerian Kesehatan berupa Gerakan Masyarakat Hidup Sehat (GERMAS), Posyandu ternyata juga mendukung program perbaikan gizi masyarakat. Dalam program posyandu, orang tua akan mendapatkan banyak pengetahuan baru tentang pendidikan dan pengetahuan dalam hal gizi serta kesehatan balita.</p>
                        <a href="http://papua.bkkbn.go.id/?p=1206#:~:text=Posyandu%20memberikan%20pelayanan%20yang%20ditujukan,melakukan%20pengecekan%20dasar%20kesehatan%20balita."> Selengkapnya </a>
                      </div>
                      </div>
                    </div>

                </p>
              </div>
            </div>

          <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection