@extends ('crud.layoutmaster')

@section ('title')
  Detail Data Balita
@endsection

@section ('content')
  <h4> {{ $post -> nama_anak }} </h4>
  <p> {{ $post -> tanggal_penimbangan }} </p>
  <p> {{ $post -> berat_badan }} </p>
  <p> {{ $post -> vitamin}} </p>
  <p> {{ $post -> imunisasi }} </p>
@endsection