@extends ('crud.layoutmaster')

@section ('title')
  Tabel Penimbangan Balita
@endsection

@section ('content')
@if (session('success'))
	<div class="alert alert-success">
		{{ session ('success')}}
	</div>
@endif
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">#</th>
      <th>Nama Anak</th>
      <th>Tanggal Penimbangan</th>
      <th>Berat Badan</th>
      <th>Vitamin</th>
      <th>Imunisasi</th>
      <th style="width: 40px">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($posts as $key => $penimbangan_balita)
    <tr>
      <td> {{ $key + 1}} </td>
      <td> {{ $penimbangan_balita -> nama_anak }} </td>
      <td> {{ $penimbangan_balita -> tanggal_penimbangan }} </td>
      <td> {{ $penimbangan_balita -> berat_badan }} </td>
      <td> {{ $penimbangan_balita -> vitamin }} </td>
      <td> {{ $penimbangan_balita -> imunisasi }} </td>
      <td style="display: flex;"> 
        <a href="/penimbangan_balita/{{$penimbangan_balita->id}}" class="btn btn-info btn-sm btn-success"> Detail </a> 
        <a href="/penimbangan_balita/{{$penimbangan_balita->id}}/edit" class="btn btn-info btn-sm btn-success btn-warning ml-2"> Edit </a>
         <form action="/penimbangan_balita/{{$penimbangan_balita->id}}" method="post">
        @csrf
        @method('DELETE')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2"> 
      </td> 

    </tr>
    @endforeach
  

</table>
@endsection




     