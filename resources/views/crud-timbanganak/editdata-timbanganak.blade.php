@extends ('crud.layoutmaster')

@section ('title')
  Penimbangan Balita
@endsection

@section ('content')
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Post Ke - {{$post->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/penimbangan_balita/{{$post->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama_anak">Nama Anak</label>
                    <input type="text" class="form-control" id="nama_anak" name="nama_anak" value="{{old('nama_anak',$post->nama_anak)}}" placeholder="Nama">
                     @error('nama_anak')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggal_penimbangan">Tanggal Penimbangan</label>
                    <input type="text" class="form-control" id="tanggal_penimbangan" name="tanggal_penimbangan" value="{{old('tanggal_penimbangan',$post->tanggal_penimbangan)}}" placeholder="Tanggal Penimbangan">
                     @error('tanggal_penimbangan')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="berat_badan">Berat Badan</label>
                    <input type="text" class="form-control" id="berat_badan" name="berat_badan" value="{{old('berat_badan',$post->berat_badan)}}" placeholder="Berat Badan">
                     @error('berat_badan')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="vitamin">Vitamin</label>
                    <select class="form-control" id="vitamin" name="vitamin" value="{{old('vitamin',$post->vitamin)}}" placeholder="Vitamin">
                          <option>Ya</option>
                          <option>Tidak</option>
                    </select>
                     @error('vitamin')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="imunisasi">Imunisasi</label>
                     <select class="form-control" id="imunisasi" name="imunisasi" value="{{old('imunisasi',$post->imunisasi)}}" placeholder="Imunisasi">
                          <option>Ya</option>
                          <option>Tidak</option>
                    </select>
                     @error('imunisasi')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                   <button type="cancel" class="btn btn-danger btn-primary">Cancel</button>
                </div>
              </form>
            </div>
  </div>

  @endsection