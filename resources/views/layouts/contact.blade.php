@extends ('layout.master')

@section ('title')
  <h1> Info Kesehatan </h1>
@endsection

 @section ('content')
 <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-20">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"></h5>

                <p class="card-text">
                  Peran posyandu di tengah masyarakat sangatlah besar. Meski identik dengan bayi dan balita, kegiatan posyandu dan manfaatnya ternyata tidak hanya sebatas itu. Banyak program posyandu yang juga diperuntukkan bagi ibu hamil, ibu menyusui, dan pasangan usia subur. <br>

Posyandu (pos pelayanan terpadu) merupakan upaya pemerintah untuk memudahkan masyarakat Indonesia dalam memperoleh pelayanan kesehatan ibu dan anak. Tujuan utama posyandu adalah mencegah peningkatan angka kematian ibu dan bayi saat kehamilan, persalinan, atau setelahnya melalui pemberdayaan masyarakat.<br>

Berbeda dengan puskesmas yang memberikan pelayanan setiap hari, posyandu hanya melayani setidaknya 1 kali dalam sebulan. Lokasi posyandu umumnya mudah dijangkau masyarakat, mulai dari lingkungan desa atau kelurahan hingga RT dan RW. <br><br><br>

Berbagai Kegiatan Posyandu dan Manfaatnya
Kegiatan posyandu terdiri dari kegiatan utama dan kegiatan pengembangan. Berikut ini adalah beberapa kegiatan utama posyandu:<br>

1. Program kesehatan ibu hamil<br>
2. Program kesehatan anak <br>
3. Keluarga Berencana (KB) <br>
4. Imunisasi <br>
5. Pemantauan status gizi <br>
6. Pencegahan dan penanggulangan diare <br>
                </p>

                <a href="https://www.alodokter.com/ini-kegiatan-posyandu-dan-manfaatnya-bagi-ibu-dan-anak" class="card-link">Baca Selengkapnya</a>
              </div>
            </div>

          <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection