@extends ('crud.layoutmaster')

@section ('title')
  Tambah Darah
@endsection

@section ('content')
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Post Ke - {{$post->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/tambah_darah/{{$post->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama',$post->nama)}}" placeholder="Nama">
                     @error('nama')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="pemberian_untuk">Pemberian Untuk</label>
                    <input type="text" class="form-control" id="pemberian_untuk" name="pemberian_untuk" value="{{old('pemberian_untuk',$post->pemberian_untuk)}}" placeholder="Pemberian Untuk">
                     @error('pemberian_untuk')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggal_pemberian">Tanggal Pemberian</label>
                    <input type="text" class="form-control" id="tanggal_pemberian" name="tanggal_pemberian" value="{{old('tanggal_pemberian',$post->tanggal_pemberian)}}" placeholder="Tanggal Pemberian">
                     @error('tanggal_pemberian')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                   <button type="cancel" class="btn btn-danger btn-primary">Cancel</button>
                </div>
              </form>
            </div>
  </div>

  @endsection