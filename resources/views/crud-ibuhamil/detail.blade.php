@extends ('crud.layoutmaster')

@section ('title')
  Detail Data Ibu Hamil
@endsection

@section ('content')
  <h4> {{ $post -> nama_ibu }} </h4>
  <p> {{ $post -> tempat_lahir }} </p>
  <p> {{ $post -> tanggal_lahir }} </p>
  <p> {{ $post -> nama_suami }} </p>
  <p> {{ $post -> alamat}} </p>
  <p> {{ $post -> tanggal_daftar }} </p>
  <p> {{ $post -> umur_kehamilan }} </p>
  <p> {{ $post -> berat_badan }} </p>
@endsection