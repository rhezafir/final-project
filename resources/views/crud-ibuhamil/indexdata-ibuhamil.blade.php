@extends ('crud.layoutmaster')

@section ('title')
  Tabel Ibu Hamil
@endsection

@section ('content')
@if (session('success'))
	<div class="alert alert-success">
		{{ session ('success')}}
	</div>
@endif
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">#</th>
      <th>Nama Ibu</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
      <th>Nama Suami</th>
      <th>Alamat</th>
      <th>Tanggal Daftar</th>
      <th>Umur Kehamilan</th>
      <th>Berat Badan</th>
      <th style="width: 40px">Action</th>
    </tr>
  </thead>
  <tbody>
     @foreach($posts as $key => $ibuhamil)
    <tr>
      <td> {{ $key + 1}} </td>
      <td> {{ $ibuhamil -> nama_ibu }} </td>
      <td> {{ $ibuhamil -> tempat_lahir }} </td>
      <td> {{ $ibuhamil -> tanggal_lahir }} </td>
      <td> {{ $ibuhamil -> nama_suami }} </td>
      <td> {{ $ibuhamil -> alamat }} </td>
      <td> {{ $ibuhamil -> tanggal_daftar }} </td>
      <td> {{ $ibuhamil -> umur_kehamilan }} </td>
      <td> {{ $ibuhamil -> berat_badan }} </td>
      <td style="display: flex;"> 
        <a href="/ibuhamil/{{$ibuhamil->id}}" class="btn btn-info btn-sm btn-success"> Detail </a> 
        <a href="/ibuhamil/{{$ibuhamil->id}}/edit" class="btn btn-info btn-sm btn-success btn-warning ml-2"> Edit </a>
        <form action="/ibuhamil/{{$ibuhamil->id}}" method="post">
        @csrf
        @method('DELETE')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2"> 
      </td> 

    </tr>
    @endforeach
  

</table>
@endsection




     