@extends ('crud.layoutmaster')

@section ('title')
  Ibu Hamil
@endsection

@section ('content')
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/ibuhamil/create" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama_ibu">Nama Ibu</label>
                    <input type="text" class="form-control" id="nama_ibu" name="nama_ibu" value="{{old('nama_ibu',$post->nama_ibu)}}" placeholder="Nama">
                     @error('nama_ibu')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir">
                     @error('tempat_lahir')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir">
                     @error('tanggal_lahir')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="nama_suami">Nama Suami</label>
                    <input type="text" class="form-control" id="nama_suami" name="nama_suami" placeholder="Nama Suami">
                     @error('nama_suami')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
                     @error('alamat')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggal_daftar">Tanggal Daftar</label>
                    <input type="text" class="form-control" id="tanggal_daftar" name="tanggal_daftar" placeholder="Tanggal Daftar">
                     @error('tanggal_daftar')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur_kehamilan">Umur Kehamilan</label>
                    <input type="text" class="form-control" id="umur_kehamilan" name="umur_kehamilan" placeholder="Umur Kehamilan">
                     @error('umur_kehamilan')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                   <div class="form-group">
                    <label for="berat_badan">Berat Badan</label>
                    <input type="text" class="form-control" id="berat_badan" name="berat_badan" placeholder="Berat Badan">
                     @error('berat_badan')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                   <button type="cancel" class="btn btn-danger btn-primary">Cancel</button>
                </div>
              </form>
            </div>
  </div>

  @endsection