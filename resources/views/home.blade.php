@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body mb-3">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
           
                    <div id="carouselExampleControls" class="carousel slide mb-3" data-ride="carousel">
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img src="img/bg1.jpg" class="d-block m-auto" width="800px" height="400px" alt="...">
                        </div>
                        <div class="carousel-item ">
                          <img src="img/bg5.jpg" class="d-block m-auto" width="800px" height="400px" alt="...">
                        </div>  
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>

                <div class="card card-primary card-outline col-ml-5">
                      <div class="card-header">
                        <h5 class="card-title m-0">News</h5>
                      </div>
                      <div class="card-body">
                        <h6 class="card-title"> Apa itu posyandu ?</h6>

                        <p class="card-text">suatu wadah komunikasi alih teknologi dalam pelayanan kesehatan masyarakat dari Keluarga Berencana dari masyarakat, oleh masyarakat dan untuk masyarakat dengan dukungan pelayanan serta pembinaan teknis dari petugas kesehatan dan keluarga. berencana yang mempunyai nilai strategis untuk pengembangan sumber daya manusia sejak dini</p>
                        <a href="https://www.kajianpustaka.com/2013/07/posyandu.html" class="btn btn-primary"> Selengkapnya </a>
                      </div>
                      </div>

                <div class="card card-primary card-outline col-ml-5">
                      <div class="card-header">
                        <h5 class="card-title m-0">Info Kesehatan</h5>
                      </div>
                      <div class="card-body">
                        <h6 class="card-title"> Apa itu posyandu ?</h6>

                        <p class="card-text">Peran posyandu di tengah masyarakat sangatlah besar. Meski identik dengan bayi dan balita, kegiatan posyandu dan manfaatnya ternyata tidak hanya sebatas itu. Banyak program posyandu yang juga diperuntukkan bagi ibu hamil, ibu menyusui, dan pasangan usia subur. <br>

                        Posyandu (pos pelayanan terpadu) merupakan upaya pemerintah untuk memudahkan masyarakat Indonesia dalam memperoleh pelayanan kesehatan ibu dan anak. Tujuan utama posyandu adalah mencegah peningkatan angka kematian ibu dan bayi saat kehamilan, persalinan, atau setelahnya melalui pemberdayaan masyarakat.<br>

                        Berbeda dengan puskesmas yang memberikan pelayanan setiap hari, posyandu hanya melayani setidaknya 1 kali dalam sebulan. Lokasi posyandu umumnya mudah dijangkau masyarakat, mulai dari lingkungan desa atau kelurahan hingga RT dan RW. <br><br><br>

                        Berbagai Kegiatan Posyandu dan Manfaatnya
                        Kegiatan posyandu terdiri dari kegiatan utama dan kegiatan pengembangan. Berikut ini adalah beberapa kegiatan utama posyandu:<br>

                        1. Program kesehatan ibu hamil<br>
                        2. Program kesehatan anak <br>
                        3. Keluarga Berencana (KB) <br>
                        4. Imunisasi <br>
                        5. Pemantauan status gizi <br>
                        6. Pencegahan dan penanggulangan diare <br></p>
                        <a href="https://www.kajianpustaka.com/2013/07/posyandu.html" class="btn btn-primary"> Selengkapnya </a>
                      </div>
                      </div>
            </div>
        </div>
    </div>
</div>
@endsection
