<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class TimbangAnakController extends Controller
{
    //
    public function create(){
    	return view('crud-timbanganak.data-timbanganak');
    }

    public function simpan(Request $request){
	$request->validate([
        "nama_anak" => 'required',
        "tanggal_penimbangan" => 'required',
        "berat_badan" => 'required',
        "vitamin" => 'required',
        "imunisasi" => 'required'
      ]);

    	$query= DB::table('penimbangan_balita')->insert([ 
    			"nama_anak" => $request["nama_anak"],
    			"tanggal_penimbangan" => $request["tanggal_penimbangan"],
    			"berat_badan" => $request["berat_badan"],
    			"vitamin" => $request["vitamin"],
    			"imunisasi" => $request["imunisasi"]
	]);

	return redirect ('/penimbangan_balita')->with('success','Data Berhasil Disimpan!');
    }

     public function index(){
   	$posts = DB::table('penimbangan_balita')->get();
   	//dd($posts);
   	return view ('crud-timbanganak.indexdata-timbanganak', compact('posts')); 
   }

   public function detail($id){
   	$post = DB::table('penimbangan_balita')->where('id', $id)->first();
   	//dd($post);
   	return view ('crud-timbanganak.detail', compact('post')); 
   }

   public function edit($id){
   	$post = DB::table('penimbangan_balita')->where('id', $id)->first();
   	//dd($post);
   	return view ('crud-timbanganak.editdata-timbanganak', compact('post')); 
   }

   public function update($id, Request $request){
  $request->validate([
        "nama_anak" => 'required',
        "tanggal_penimbangan" => 'required',
        "berat_badan" => 'required',
        "vitamin" => 'required',
        "imunisasi" => 'required'
      ]);

      $query = DB::table('penimbangan_balita')
                ->where('id', $id)
                ->update([ 
          "nama_anak" => $request["nama_anak"],
          "tanggal_penimbangan" => $request["tanggal_penimbangan"],
          "berat_badan" => $request["berat_badan"],
          "vitamin" => $request["vitamin"],
          "imunisasi" => $request["imunisasi"]
  ]);

  return redirect ('/penimbangan_balita')->with('success','Data Berhasil Diubah!');
    }

   public function destroy($id){
      $query = DB::table('penimbangan_balita')->where('id', $id)->delete(); 
      //dd($query);
      return redirect('/penimbangan_balita')->with ('success','Berhasil Hapus Data');  
    }

}
