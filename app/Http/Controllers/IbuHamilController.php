<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class IbuHamilController extends Controller
{
    //
    public function create(){
    	return view('crud-ibuhamil.data-ibuhamil');
    }

    public function simpan(Request $request){
	$request->validate([
        "nama_ibu" => 'required',
        "tempat_lahir" => 'required',
        "tanggal_lahir" => 'required',
        "nama_suami" => 'required',
        "alamat" => 'required',
        "tanggal_daftar" => 'required',
        "umur_kehamilan" => 'required',
        "berat_badan" => 'required'
      ]);

    	$query= DB::table('ibuhamil')->insert([ 
    			"nama_ibu" => $request["nama_ibu"],
    			"tempat_lahir" => $request["tempat_lahir"],
    			"tanggal_lahir" => $request["tanggal_lahir"],
    			"nama_suami" => $request["nama_suami"],
    			"alamat" => $request["alamat"],
    			"tanggal_daftar" => $request["tanggal_daftar"],
    			"umur_kehamilan" => $request["umur_kehamilan"],
    			"berat_badan" => $request["berat_badan"]
	]);

	return redirect ('/ibuhamil')->with('success','Data Berhasil Disimpan!');
    }

    public function index(){
   	$posts = DB::table('ibuhamil')->get();
   	//dd($posts);
   	return view ('crud-ibuhamil.indexdata-ibuhamil', compact('posts')); 
   }

   public function detail($id){
   	$post = DB::table('ibuhamil')->where('id', $id)->first();
   	//dd($post);
   	return view ('crud-ibuhamil.detail', compact('post')); 
   }

   public function edit($id){
   	$post = DB::table('ibuhamil')->where('id', $id)->first();
   	//dd($post);
   	return view ('crud-ibuhamil.editdata-ibuhamil', compact('post')); 
   }

   public function update($id, Request $request){
	$request->validate([
        "nama_ibu" => 'required',
        "tempat_lahir" => 'required',
        "tanggal_lahir" => 'required',
        "nama_suami" => 'required',
        "alamat" => 'required',
        "tanggal_daftar" => 'required',
        "umur_kehamilan" => 'required',
        "berat_badan" => 'required'
      ]);

    	$query = DB::table('ibuhamil')
    						->where('id', $id)
    						->update([ 
    			"nama_ibu" => $request["nama_ibu"],
    			"tempat_lahir" => $request["tempat_lahir"],
    			"tanggal_lahir" => $request["tanggal_lahir"],
    			"nama_suami" => $request["nama_suami"],
    			"alamat" => $request["alamat"],
    			"tanggal_daftar" => $request["tanggal_daftar"],
    			"umur_kehamilan" => $request["umur_kehamilan"],
    			"berat_badan" => $request["berat_badan"]
	]);

	return redirect ('/ibuhamil')->with('success','Data Berhasil Diubah!');
    }

    public function destroy($id){
    	$query = DB::table('ibuhamil')->where('id', $id)->delete(); 
    	//dd($query);
    	return redirect('/ibuhamil')->with ('success','Berhasil Hapus Data');  
    }

}
