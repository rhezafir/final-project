<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class TambahDarahController extends Controller
{
    //
    public function create(){
    	return view('crud-tambahdarah.data-tambahdarah');
    }

    public function simpan(Request $request){
	$request->validate([
        "nama" => 'required',
        "pemberian_untuk" => 'required',
        "tanggal_pemberian" => 'required'
      ]);

    	$query= DB::table('tambah_darah')->insert([ 
    			"nama" => $request["nama"],
    			"pemberian_untuk" => $request["pemberian_untuk"],
    			"tanggal_pemberian" => $request["tanggal_pemberian"]
	]);

	return redirect ('/tambah_darah')->with('success','Data Berhasil Disimpan!');
    }

     public function index(){
   	$posts = DB::table('tambah_darah')->get();
   	//dd($posts);
   	return view ('crud-tambahdarah.indexdata-tambahdarah', compact('posts')); 
   }

   public function detail($id){
   	$post = DB::table('tambah_darah')->where('id', $id)->first();
   	//dd($post);
   	return view ('crud-tambahdarah.detail', compact('post')); 
   }

   public function edit($id){
   	$post = DB::table('tambah_darah')->where('id', $id)->first();
   	//dd($post);
   	return view ('crud-tambahdarah.editdata-tambahdarah', compact('post')); 
   }

  public function update($id, Request $request){
  $request->validate([
        "nama" => 'required',
        "pemberian_untuk" => 'required',
        "tanggal_pemberian" => 'required'
      ]);

      $query = DB::table('tambah_darah')
                ->where('id', $id)
                ->update([ 
         		"nama" => $request["nama"],
    			"pemberian_untuk" => $request["pemberian_untuk"],
    			"tanggal_pemberian" => $request["tanggal_pemberian"]
  ]);

  return redirect ('/tambah_darah')->with('success','Data Berhasil Diubah!');
    }

    public function destroy($id){
      $query = DB::table('tambah_darah')->where('id', $id)->delete(); 
      //dd($query);
      return redirect('/tambah_darah')->with ('success','Berhasil Hapus Data');  
    }
}
