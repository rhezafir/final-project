<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/home','HomeController@home');
Route::get('/contact','HomeController@contact');
Route::get('/aboutus','HomeController@aboutus');
Route::get('/login','HomeController@login');
Route::get('/dashboarddata', function () {
    return view('crud.layoutmaster');
});
Route::get('/balita/create','postController@create');
Route::post('/balita/create','postController@simpan');
Route::get('/balita','postController@index');
Route::get('/balita/{id}','postController@detail');
Route::get('/balita/{id}/edit','postController@edit');
Route::put('/balita/{id}','postController@update');
Route::delete('/balita/{id}','postController@destroy');

Route::get('/ibuhamil/create','IbuHamilController@create');
Route::post('/ibuhamil/create','IbuHamilController@simpan');
Route::get('/ibuhamil','IbuHamilController@index');
Route::get('/ibuhamil/{id}','IbuHamilController@detail');
Route::get('/ibuhamil/{id}/edit','IbuHamilController@edit');
Route::put('/ibuhamil/{id}','IbuHamilController@update');
Route::delete('/ibuhamil/{id}','postController@destroy');

Route::get('/penimbangan_balita/create','TimbangAnakController@create');
Route::post('/penimbangan_balita/create','TimbangAnakController@simpan');
Route::get('/penimbangan_balita','TimbangAnakController@index');
Route::get('/penimbangan_balita/{id}','TimbangAnakController@detail');
Route::get('/penimbangan_balita/{id}/edit','TimbangAnakController@edit');
Route::put('/penimbangan_balita/{id}','TimbangAnakController@update');
Route::delete('/penimbangan_balita/{id}','TimbangAnakController@destroy');

Route::get('/tambah_darah/create','TambahDarahController@create');
Route::post('/tambah_darah/create','TambahDarahController@simpan');
Route::get('/tambah_darah','TambahDarahController@index');
Route::get('/tambah_darah/{id}','TambahDarahController@detail');
Route::get('/tambah_darah/{id}/edit','TambahDarahController@edit');
Route::put('/tambah_darah/{id}','TambahDarahController@update');
Route::delete('/tambah_darah/{id}','TambahDarahController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
